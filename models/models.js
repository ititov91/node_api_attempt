const sequelize = require('../db')

const {DataTypes} = require('sequelize')

const User = sequelize.define('user', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    email: {type: DataTypes.STRING, unique: true},
    password: {type: DataTypes.STRING},
    role: {type: DataTypes.STRING, defaultValue: "USER"},
})

const Cart = sequelize.define('cart', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true}
})

const PeepoCart = sequelize.define('peepocart', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true}
})

const Peepo = sequelize.define('peepo', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    name: {type: DataTypes.STRING, unique: true, allowNull: false},
    desc: {type: DataTypes.STRING, allowNull: false},
    image: {type: DataTypes.STRING, allowNull: false},
    price: {type: DataTypes.INTEGER, allowNull: false},
    rating: {type: DataTypes.INTEGER, defaultValue: 0}
})

const Rating = sequelize.define('rating', {
    id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    rating: {type: DataTypes.INTEGER, allowNull: false}
})


User.hasOne(Cart)
Cart.belongsTo(User)

Peepo.hasMany(Rating)
Rating.belongsTo(Peepo)

Peepo.hasMany(PeepoCart)
PeepoCart.belongsTo(Peepo)

module.exports = {
    User,
    Cart,
    PeepoCart,
    Rating,
    Peepo
}

