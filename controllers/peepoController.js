const {Peepo} = require('../models/models')
const ApiError = require('../Error/APIError')



class PeepoController{

    async create(req, res, next){

        try {
            const {name, desc, image, price} = req.body
            const peepo = await Peepo.create({name, desc, image, price})
            return res.json(peepo)
        }
        catch (e){
            next(ApiError.BadRequest('very bad'))
        }
    } 
    

    async getAll(req, res){
        const peepos = await Peepo.findAll()
        return res.json(peepos)
    }

    async getSingle(req, res){
        const {id} = req.params
        const peepo = await Peepo.findOne({
            where: {id}
        })
        return res.json(peepo)

    }
}

module.exports = new PeepoController()