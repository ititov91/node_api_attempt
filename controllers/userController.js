const ApiError = require('../Error/APIError')
const bcrypt = require('bcrypt')
const {User, Cart} = require('../models/models')
const jwt = require('jsonwebtoken')


const generateJwt = (id, email, role) =>{
    return jwt.sign(
        {id: id, email, role},
        process.env.SECRET_KEY,
        {expiresIn: '48h'}
    )
} 

class UserController{

    async registration(req, res, next){

        const {email, password, role} = req.body
        if (!email || !password){
            return next(ApiError.BadRequest('Некорректно указана почта/пароль.'))
        }

        const candidate = await User.findOne({where: {email}})
        if (candidate){
            return next(ApiError.BadRequest('Email занят.'))
        }

        const hashPassword = await bcrypt.hash(password, 7)
        const user = await User.create({email, role, password: hashPassword})
        const cart = await Cart.create({userId: user.id})
        //разобраться с импортом жвт - не работает.
        const token = generateJwt(user.id, user.email, user.role)
        return res.json({token})
    }

    async login (req, res, next){
        const{email, password} = req.body
        const user = await User.findOne({where:{email}})
        if (!user) {
            return next(ApiError.BadRequest('tebya net'))
        }
        let comparePasswords = bcrypt.compareSync(password, user.password)
        if (!comparePasswords){
            return next(ApiError.BadRequest('nepravilniy POROL'))
        }
        const token = generateJwt(user.id, user.email, user.role)
        return res.json({token})

    }

    async check (req, res, next){
        res.json({message: 'DOBRO POJALOVATSYA'})

    }

    
}

module.exports = new UserController()