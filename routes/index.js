const Router = require('express')
const router = new Router()
const peepoRouter = require('./peepoRouter')
const userRouter = require('./userRouter')


router.use('/user', userRouter)
router.use('/peepo', peepoRouter)


module.exports = router
