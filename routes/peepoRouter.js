const Router = require('express')
const peepoController = require('../controllers/peepoController')
const router = new Router()
const PeepoController = require('../controllers/peepoController')


router.post('/', peepoController.create)
router.get('/', peepoController.getAll)
router.get('/:id', peepoController.getSingle)


module.exports = router